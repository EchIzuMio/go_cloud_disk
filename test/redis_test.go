package test

import (
	"context"
	"github.com/go-redis/redis/v8"
	"log"
	"testing"
	"time"
)

var ctx = context.Background()

var rdb = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "111111",
	DB:       0,
})

func TestRedisSet(t *testing.T) {
	err := rdb.Set(ctx, "name11", "cjl10111111", time.Second*60).Err()

	if err != nil {
		log.Println("Redis Set is Wrong ! " + err.Error())
	}
}

func TestGetValue(t *testing.T) {
	val, err := rdb.Get(ctx, "name11").Result()
	if err != nil {
		t.Error(err)
	}
	t.Logf(val)
}
