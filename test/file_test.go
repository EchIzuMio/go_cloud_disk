package test

import (
	"bytes"
	"cloud-disk/core/define"
	"context"
	"crypto/md5"
	"fmt"
	"github.com/tencentyun/cos-go-sdk-v5"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"testing"
)

const ChunkSize = 1024 * 1024 //100MB
var testPhoto = "DSC_2134.JPG"
var testVideo = "videoTest.flv"

func TestGenerateChunkFile(t *testing.T) {
	fileInfo, err := os.Stat(testPhoto)
	if err != nil {
		t.Fatal(err)
	}
	chunkNum := math.Ceil(float64(fileInfo.Size()) / float64(ChunkSize))
	myFile, err := os.OpenFile(testPhoto, os.O_RDONLY, 0666)
	if err != nil {
		t.Fatal(err)
	}
	b := make([]byte, ChunkSize)
	for i := 0; i < int(chunkNum); i++ {
		myFile.Seek(int64(i*ChunkSize), 0)
		if ChunkSize > fileInfo.Size()-int64(i+ChunkSize) {
			b = make([]byte, fileInfo.Size()-int64(i*ChunkSize))
		}
		myFile.Read(b)
		f, err := os.OpenFile("./"+strconv.Itoa(i)+".chunk", os.O_CREATE|os.O_WRONLY, os.ModePerm)
		if err != nil {
			t.Fatal(err)
		}
		f.Write(b)
		f.Close()
		//err = ioutil.WriteFile(fmt.Sprintf("chunk_%d.flv",i),b,0666)
		//err = os.WriteFile(fmt.Sprintf("chunk_%d.flv",i),b,0666)
		//if err  != nil{
		//	t.Fatal(err)
		//}
	}
	myFile.Close()
}

func TestMergeChunkFile(t *testing.T) {
	myFile, err := os.OpenFile("test.JPG", os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
	if err != nil {
		return
	}
	fileInfo, err := os.Stat(testPhoto)
	if err != nil {
		t.Fatal(err)
	}
	chunkNum := math.Ceil(float64(fileInfo.Size()) / float64(ChunkSize))
	for i := 0; i < int(chunkNum); i++ {
		f, err := os.OpenFile("./"+strconv.Itoa(i)+".chunk", os.O_RDONLY, os.ModePerm)
		if err != nil {
			t.Fatal(err)
		}
		b, err := io.ReadAll(f)
		if err != nil {
			t.Fatal(err)
		}
		myFile.Write(b)
		f.Close()

	}
	myFile.Close()
}

func TestCheck(t *testing.T) {
	testFile, err := os.OpenFile("test.JPG", os.O_RDONLY, 0666)
	if err != nil {
		return
	}
	b1, err := ioutil.ReadAll(testFile)
	if err != nil {
		return
	}
	file2, err := os.OpenFile(testPhoto, os.O_RDONLY, 0666)
	if err != nil {
		t.Fatal(err)
	}
	b2, err := ioutil.ReadAll(file2)
	if err != nil {
		t.Fatal(err)
	}
	s1 := fmt.Sprintf("%x", md5.Sum(b1))
	s2 := fmt.Sprintf("%x", md5.Sum(b2))
	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s1 == s2)
}

const COS_ADDRESS = "https://cjl-cloud-disk-1314752272.cos.ap-nanjing.myqcloud.com"

// 分片上传初始化
func TestInitPartUpload(t *testing.T) {
	u, _ := url.Parse(COS_ADDRESS)
	b := &cos.BaseURL{BucketURL: u}
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  define.TencentSecretID,
			SecretKey: define.TencentSecretKey,
		},
	})
	key := "cloud-disk/exampleobject.jpeg"
	v, _, err := client.Object.InitiateMultipartUpload(context.Background(), key, nil)
	if err != nil {
		t.Fatal(err)
	}
	UploadID := v.UploadID // 16795544154372ea9e00da32f9ce6eb53698404d176fef8679e2d0b32362750304ed721d89
	fmt.Println(UploadID)
}

// 分片上传
func TestPartUpload(t *testing.T) {
	u, _ := url.Parse(COS_ADDRESS)
	b := &cos.BaseURL{BucketURL: u}
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  define.TencentSecretID,
			SecretKey: define.TencentSecretKey,
		},
	})
	key := "cloud-disk/exampleobject.jpeg"
	UploadID := "16795544154372ea9e00da32f9ce6eb53698404d176fef8679e2d0b32362750304ed721d89"
	f, err := os.ReadFile("0.chunk") // md5 : 108e92d35fe1695fbf29737d0b24561d
	if err != nil {
		t.Fatal(err)
	}
	// opt可选
	resp, err := client.Object.UploadPart(
		context.Background(), key, UploadID, 1, bytes.NewReader(f), nil,
	)
	if err != nil {
		t.Fatal(err)
	}
	PartETag := resp.Header.Get("ETag")
	fmt.Println(PartETag)
}

// 分片上传完成
func TestPartUploadComplete(t *testing.T) {
	u, _ := url.Parse(COS_ADDRESS)
	b := &cos.BaseURL{BucketURL: u}
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  define.TencentSecretID,
			SecretKey: define.TencentSecretKey,
		},
	})
	key := "cloud-disk/exampleobject.jpeg"
	UploadID := "16795544154372ea9e00da32f9ce6eb53698404d176fef8679e2d0b32362750304ed721d89"

	opt := &cos.CompleteMultipartUploadOptions{}
	opt.Parts = append(opt.Parts, cos.Object{ //bca6864cf7ff6d08d5b9d9dd52826049
		PartNumber: 1, ETag: "bca6864cf7ff6d08d5b9d9dd52826049"},
	)
	_, _, err := client.Object.CompleteMultipartUpload(
		context.Background(), key, UploadID, opt,
	)
	if err != nil {
		t.Fatal(err)
	}
}
