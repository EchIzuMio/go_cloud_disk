package logic

import (
	"cloud-disk/core/define"
	"cloud-disk/core/models"
	"cloud-disk/core/utils"
	"context"
	"errors"
	"log"
	"time"

	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type CodeSendRegisterLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewCodeSendRegisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CodeSendRegisterLogic {
	return &CodeSendRegisterLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *CodeSendRegisterLogic) CodeSendRegister(req *types.SendCodeRequest) (resp *types.SendCodeReply, err error) {

	cnt, err := l.svcCtx.Engine.Where("name = ?", req.Name).Count(new(models.UserBasic))
	if err != nil {
		return nil, err
	}
	if cnt > 0 {
		err = errors.New("用户已存在！")
		return nil, err
	}
	resp = &types.SendCodeReply{}
	code := utils.SendCode()
	//userCode := code + req.Name
	log.Println("user {" + req.Name + "} code is : " + code)
	l.svcCtx.RDB.Set(l.ctx, define.REGISTER_USER_KEY+req.Name, code, time.Second*300)
	resp.Code = code
	return resp, nil
}
