package logic

import (
	"context"

	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type SharedBasicDetailLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewSharedBasicDetailLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SharedBasicDetailLogic {
	return &SharedBasicDetailLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SharedBasicDetailLogic) SharedBasicDetail(req *types.SharedBasicDetailRequest) (resp *types.SharedBasicDetailReply, err error) {
	// todo: add your logic here and delete this line
	_, err = l.svcCtx.Engine.Exec("update share_basic set click_num = click_num + 1 where identity = ?", req.Identity)
	if err != nil {
		return nil, err
	}
	resp = new(types.SharedBasicDetailReply)
	_, err = l.svcCtx.Engine.Table("share_basic").
		Select("share_basic.repository_identity,repository_pool.name,repository_pool.ext,repository_pool.path").
		Join("left", "repository_pool", "share_basic.repository_identity = repository_pool.identity").
		Join("left", "user_repository", "user_repository.identity = share_basic.user_repository_identity").
		Where("share_basic.identity = ?", req.Identity).Get(resp)
	if err != nil {
		return nil, err
	}
	return
}
