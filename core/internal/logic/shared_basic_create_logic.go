package logic

import (
	"cloud-disk/core/models"
	"cloud-disk/core/utils"
	"context"
	"errors"

	"cloud-disk/core/internal/svc"
	"cloud-disk/core/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type SharedBasicCreateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewSharedBasicCreateLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SharedBasicCreateLogic {
	return &SharedBasicCreateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SharedBasicCreateLogic) SharedBasicCreate(req *types.SharedBasicCreateRequest, userIdentity string) (resp *types.UserBasicCreateReply, err error) {
	// todo: add your logic here and delete this line
	uuid := utils.GetUUID()
	ur := new(models.UserRepository)
	has, err := l.svcCtx.Engine.Where("identity = ?", req.UserRepositoryIdentity).Get(ur)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, errors.New("user repository not found ")
	}
	data := new(models.ShareBasic)
	data.Identity = uuid
	data.UserIdentity = userIdentity
	data.UserRepositoryIdentity = req.UserRepositoryIdentity
	data.RepositoryIdentity = ur.RepositoryIdentity
	data.ExpiredTime = req.ExpiredTime
	_, err = l.svcCtx.Engine.Insert(data)
	if err != nil {
		return nil, err
	}
	resp = &types.UserBasicCreateReply{
		Identity: uuid,
	}
	return
}
