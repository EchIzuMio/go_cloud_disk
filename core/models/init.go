package models

import (
	"github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"xorm.io/xorm"
)

//var Engine = Init("root:111111@tcp(127.0.0.1:3306)/cloud-disk?charset=utf8mb4&parseTime=True&loc=Local")
//var RDB = InitRedis()

func Init(dataSource string) *xorm.Engine {
	engine, err := xorm.NewEngine("mysql", dataSource)
	if err != nil {
		log.Printf("Xorm New Engine Error:%v", err)
		return nil
	}
	return engine
}

//func Init(dataSource string) *xorm.Engine {
//	engine, err := xorm.NewEngine("mysql", dataSource)
//	if err != nil {
//		log.Printf("Xorm New Engine Error:%v", err)
//		return nil
//	}
//	return engine
//}

func InitRedis(addr string) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "111111", // no password set
		DB:       0,        // use default DB
	})
}
