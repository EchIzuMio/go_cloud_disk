package define

import (
	"github.com/dgrijalva/jwt-go"
)

type UserClaim struct {
	Id       int
	Identity string
	Name     string
	jwt.StandardClaims
}

var JwtKey = "cloud-disk-key"

var REGISTER_USER_KEY = "register:user:"

var TencentSecretKey = "3tfMzX4exIbq4FSn5IVq8Y5hQF867FQe"
var TencentSecretID = "AKIDwOoLoFaq052rgR51pU1TmwQqfGgS0hRd"
var CosBucketDomain = "https://cjl-cloud-disk-1314752272.cos.ap-nanjing.myqcloud.com"

var PageSize = 20

var DateTime = "YYYY-MM-dd HH:mm:ss"

var TokenExpire = 3600
var RefreshToken = 300
